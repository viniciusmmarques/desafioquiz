var placar=0; //variavel que vai receber a pontuação das questões
var questao = -1;//seletor das perguntas para saber a hora de finalizar o quiz
var perguntas = [
    {

        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' }
        ]
    },
    {

        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {

        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {

        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {

        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
    },
    {
        texto: 'Qual músico morreu em 2017',
        respostas: [
            { valor: 3, texto: 'Chester Bennington' },
            { valor: 0, texto: 'Kurt Cobain' },
            { valor: 1, texto: 'Avicii' },
            { valor: 2, texto: 'David Bowie' }]
    }
]
function mostrarQuestao() // função que vai atualizar as questões e checar se a questão foi respondida para poder prosseguir
{
    var answer=document.getElementsByName("resposta");//essa vai ser  a variavel que vai receber o valor dos list item para ser checado
    if(answer[0].checked  != false  ||  answer[1].checked  != false  ||  answer[2].checked  !=  false  ||  answer[3].checked  !=  false  ||  questao ==-1 ){//esse if é para checar sem a questão foi respondida
        if(questao != -1){
            for(var i=0;i<4;i++){
            placar += answer[i].checked*perguntas[questao]['respostas'][i]['valor'];//atualiza o placar
            }
                        }
        document.getElementById('resultado').innerHTML = "";//a div fica sem conteúdo
        document.getElementById('confirmar').innerHTML = 'Próxima';//o conteúdo dentro do html do botão vai ser alterado de começar para próxima.
        questao ++;// o seletor de questoes é incrementado 
    
        }                             
    if(questao<=5)
    {

     document.getElementById('titulo').innerHTML = perguntas[questao]['texto'];//o conteudo do h1 recebe o nome da pergunta atual
     document.getElementById("listaRespostas").style.display = 'block';//mostra as opções para marcar
     for(var i = 0; i < 4; i++){//atualiza as questões
            answer[i].checked = false;// é atribuido  false para não mostrar o resultado anterior
            answer[i].value = perguntas[questao]['respostas'][i]['valor'];//define o valor da resposta
            document.getElementsByTagName('span')[i].innerHTML = perguntas[questao]['respostas'][i]['texto'];//escreve as perguntas      
        }
    }
    else{

        finalizarQuiz();//a função finalizarQuiz é chamada
    
    }
}
function finalizarQuiz() {
    document.getElementById('titulo').innerHTML = 'Resultado';//o h1 tem o conteudo alterado para "resultado"
    document.getElementById('listaRespostas').style.display = 'none';//a unordered list fica invisivel
    document.getElementById('resultado').innerHTML = "Você conseguiu:  " + ((placar/18) * 100).toFixed(2) + "%";//exibe o placar atingido
    document.getElementById('confirmar').innerHTML = 'tente outra vez';//o conteudo de dentro do botão é alterado novamente
    placar = 0;   //Condições iniciais restabelecidas
    questao = -1; //Condições iniciais restabelecidas
}
